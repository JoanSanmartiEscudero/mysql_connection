
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Main {

    
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/universidad?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","root");
            System.out.println("¡Conexión realizada con exito!");
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en la conexión de la base de datos: " + ex.getMessage());
        }
    }
    
}
